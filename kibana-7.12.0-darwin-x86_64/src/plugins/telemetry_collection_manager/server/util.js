"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isClusterOptedIn = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0 and the Server Side Public License, v 1; you may not use this file except
 * in compliance with, at your election, the Elastic License 2.0 or the Server
 * Side Public License, v 1.
 */
const isClusterOptedIn = clusterUsage => {
  var _clusterUsage$stack_s, _clusterUsage$stack_s2, _clusterUsage$stack_s3, _clusterUsage$stack_s4;

  return (clusterUsage === null || clusterUsage === void 0 ? void 0 : (_clusterUsage$stack_s = clusterUsage.stack_stats) === null || _clusterUsage$stack_s === void 0 ? void 0 : (_clusterUsage$stack_s2 = _clusterUsage$stack_s.kibana) === null || _clusterUsage$stack_s2 === void 0 ? void 0 : (_clusterUsage$stack_s3 = _clusterUsage$stack_s2.plugins) === null || _clusterUsage$stack_s3 === void 0 ? void 0 : (_clusterUsage$stack_s4 = _clusterUsage$stack_s3.telemetry) === null || _clusterUsage$stack_s4 === void 0 ? void 0 : _clusterUsage$stack_s4.opt_in_status) === true;
};

exports.isClusterOptedIn = isClusterOptedIn;