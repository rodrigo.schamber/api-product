"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createStubs = createStubs;

var _sinon = _interopRequireDefault(require("sinon"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}
/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0; you may not use this file except in compliance with the Elastic License
 * 2.0.
 */


function createStubs(mockQueryResult, featureStub) {
  const callWithRequestStub = _sinon.default.stub().returns(Promise.resolve(mockQueryResult));

  const getClusterStub = _sinon.default.stub().returns({
    callWithRequest: callWithRequestStub
  });

  const configStub = _sinon.default.stub().returns({
    get: _sinon.default.stub().withArgs('xpack.monitoring.cluster_alerts.enabled').returns(true)
  });

  return {
    callWithRequestStub,
    mockReq: {
      server: {
        config: configStub,
        plugins: {
          monitoring: {
            info: {
              feature: featureStub
            }
          },
          elasticsearch: {
            getCluster: getClusterStub
          }
        }
      }
    }
  };
}