"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkRunningSessions = checkRunningSessions;

var _moment = _interopRequireDefault(require("moment"));

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _common = require("../../../../../../src/plugins/data/common");

var _common2 = require("../../../common");

var _get_search_status = require("./get_search_status");

var _get_session_status = require("./get_session_status");

var _types = require("./types");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}
/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0; you may not use this file except in compliance with the Elastic License
 * 2.0.
 */


function isSessionStale(session, config, logger) {
  const curTime = (0, _moment.default)(); // Delete if a running session wasn't polled for in the last notTouchedInProgressTimeout OR
  // if a completed \ errored \ canceled session wasn't saved for within notTouchedTimeout

  return session.attributes.status === _common2.SearchSessionStatus.IN_PROGRESS && curTime.diff((0, _moment.default)(session.attributes.touched), 'ms') > config.notTouchedInProgressTimeout.asMilliseconds() || session.attributes.status !== _common2.SearchSessionStatus.IN_PROGRESS && curTime.diff((0, _moment.default)(session.attributes.touched), 'ms') > config.notTouchedTimeout.asMilliseconds();
}

async function updateSessionStatus(session, client, logger) {
  let sessionUpdated = false; // Check statuses of all running searches

  await Promise.all(Object.keys(session.attributes.idMapping).map(async searchKey => {
    const updateSearchRequest = currentStatus => {
      sessionUpdated = true;
      session.attributes.idMapping[searchKey] = { ...session.attributes.idMapping[searchKey],
        ...currentStatus
      };
    };

    const searchInfo = session.attributes.idMapping[searchKey];

    if (searchInfo.status === _types.SearchStatus.IN_PROGRESS) {
      try {
        const currentStatus = await (0, _get_search_status.getSearchStatus)(client, searchInfo.id);

        if (currentStatus.status !== searchInfo.status) {
          logger.debug(`search ${searchInfo.id} | status changed to ${currentStatus.status}`);
          updateSearchRequest(currentStatus);
        }
      } catch (e) {
        var _e$meta$error, _e$meta$error$caused_;

        logger.error(e);
        updateSearchRequest({
          status: _types.SearchStatus.ERROR,
          error: e.message || ((_e$meta$error = e.meta.error) === null || _e$meta$error === void 0 ? void 0 : (_e$meta$error$caused_ = _e$meta$error.caused_by) === null || _e$meta$error$caused_ === void 0 ? void 0 : _e$meta$error$caused_.reason)
        });
      }
    }
  })); // And only then derive the session's status

  const sessionStatus = (0, _get_session_status.getSessionStatus)(session.attributes);

  if (sessionStatus !== session.attributes.status) {
    session.attributes.status = sessionStatus;
    session.attributes.touched = new Date().toISOString();
    sessionUpdated = true;
  }

  return sessionUpdated;
}

function getSavedSearchSessionsPage$({
  savedObjectsClient,
  logger
}, config, page) {
  logger.debug(`Fetching saved search sessions page ${page}`);
  return (0, _rxjs.from)(savedObjectsClient.find({
    page,
    perPage: config.pageSize,
    type: _common2.SEARCH_SESSION_TYPE,
    namespaces: ['*'],
    filter: _common.nodeBuilder.or([_common.nodeBuilder.and([_common.nodeBuilder.is(`${_common2.SEARCH_SESSION_TYPE}.attributes.status`, _common2.SearchSessionStatus.IN_PROGRESS.toString()), _common.nodeBuilder.is(`${_common2.SEARCH_SESSION_TYPE}.attributes.persisted`, 'true')]), _common.nodeBuilder.is(`${_common2.SEARCH_SESSION_TYPE}.attributes.persisted`, 'false')])
  }));
}

function getAllSavedSearchSessions$(deps, config) {
  return getSavedSearchSessionsPage$(deps, config, 1).pipe((0, _operators.expand)(result => {
    if (!result || !result.saved_objects || result.saved_objects.length < config.pageSize) return _rxjs.EMPTY;else {
      return getSavedSearchSessionsPage$(deps, config, result.page + 1);
    }
  }));
}

async function checkRunningSessions(deps, config) {
  const {
    logger,
    client,
    savedObjectsClient
  } = deps;

  try {
    await getAllSavedSearchSessions$(deps, config).pipe((0, _operators.mergeMap)(async runningSearchSessionsResponse => {
      if (!runningSearchSessionsResponse.total) return;
      logger.debug(`Found ${runningSearchSessionsResponse.total} running sessions`);
      const updatedSessions = new Array();
      await Promise.all(runningSearchSessionsResponse.saved_objects.map(async session => {
        const updated = await updateSessionStatus(session, client, logger);
        let deleted = false;

        if (!session.attributes.persisted) {
          if (isSessionStale(session, config, logger)) {
            deleted = true; // delete saved object to free up memory
            // TODO: there's a potential rare edge case of deleting an object and then receiving a new trackId for that same session!
            // Maybe we want to change state to deleted and cleanup later?

            logger.debug(`Deleting stale session | ${session.id}`);
            await savedObjectsClient.delete(_common2.SEARCH_SESSION_TYPE, session.id); // Send a delete request for each async search to ES

            Object.keys(session.attributes.idMapping).map(async searchKey => {
              const searchInfo = session.attributes.idMapping[searchKey];

              if (searchInfo.strategy === _common2.ENHANCED_ES_SEARCH_STRATEGY) {
                try {
                  await client.asyncSearch.delete({
                    id: searchInfo.id
                  });
                } catch (e) {
                  logger.debug(`Error ignored while deleting async_search ${searchInfo.id}: ${e.message}`);
                }
              }
            });
          }
        }

        if (updated && !deleted) {
          updatedSessions.push(session);
        }
      })); // Do a bulk update

      if (updatedSessions.length) {
        // If there's an error, we'll try again in the next iteration, so there's no need to check the output.
        const updatedResponse = await savedObjectsClient.bulkUpdate(updatedSessions);
        logger.debug(`Updated ${updatedResponse.saved_objects.length} search sessions`);
      }
    })).toPromise();
  } catch (err) {
    logger.error(err);
  }
}