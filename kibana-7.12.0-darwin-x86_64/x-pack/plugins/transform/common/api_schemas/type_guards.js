"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isStopTransformsResponseSchema = exports.isStartTransformsResponseSchema = exports.isPutTransformsResponseSchema = exports.isPostTransformsUpdateResponseSchema = exports.isPostTransformsPreviewResponseSchema = exports.isGetTransformsAuditMessagesResponseSchema = exports.isFieldHistogramsResponseSchema = exports.isEsSearchResponse = exports.isEsIndices = exports.isDeleteTransformsResponseSchema = exports.isGetTransformsStatsResponseSchema = exports.isGetTransformsResponseSchema = void 0;
/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0; you may not use this file except in compliance with the Elastic License
 * 2.0.
 */
// To be able to use the type guards on the client side, we need to make sure we don't import
// the code of '@kbn/config-schema' but just its types, otherwise the client side code will
// fail to build.

const isBasicObject = arg => {
  return typeof arg === 'object' && arg !== null;
};

const isGenericResponseSchema = arg => {
  return isBasicObject(arg) && {}.hasOwnProperty.call(arg, 'count') && {}.hasOwnProperty.call(arg, 'transforms') && Array.isArray(arg.transforms);
};

const isGetTransformsResponseSchema = arg => {
  return isGenericResponseSchema(arg);
};

exports.isGetTransformsResponseSchema = isGetTransformsResponseSchema;

const isGetTransformsStatsResponseSchema = arg => {
  return isGenericResponseSchema(arg);
};

exports.isGetTransformsStatsResponseSchema = isGetTransformsStatsResponseSchema;

const isDeleteTransformsResponseSchema = arg => {
  return isBasicObject(arg) && Object.values(arg).every(d => ({}).hasOwnProperty.call(d, 'transformDeleted'));
};

exports.isDeleteTransformsResponseSchema = isDeleteTransformsResponseSchema;

const isEsIndices = arg => {
  return Array.isArray(arg);
};

exports.isEsIndices = isEsIndices;

const isEsSearchResponse = arg => {
  return isBasicObject(arg) && {}.hasOwnProperty.call(arg, 'hits');
};

exports.isEsSearchResponse = isEsSearchResponse;

const isFieldHistogramsResponseSchema = arg => {
  return Array.isArray(arg);
};

exports.isFieldHistogramsResponseSchema = isFieldHistogramsResponseSchema;

const isGetTransformsAuditMessagesResponseSchema = arg => {
  return Array.isArray(arg);
};

exports.isGetTransformsAuditMessagesResponseSchema = isGetTransformsAuditMessagesResponseSchema;

const isPostTransformsPreviewResponseSchema = arg => {
  return isBasicObject(arg) && {}.hasOwnProperty.call(arg, 'generated_dest_index') && {}.hasOwnProperty.call(arg, 'preview') && typeof arg.generated_dest_index !== undefined && Array.isArray(arg.preview);
};

exports.isPostTransformsPreviewResponseSchema = isPostTransformsPreviewResponseSchema;

const isPostTransformsUpdateResponseSchema = arg => {
  return isBasicObject(arg) && {}.hasOwnProperty.call(arg, 'id') && typeof arg.id === 'string';
};

exports.isPostTransformsUpdateResponseSchema = isPostTransformsUpdateResponseSchema;

const isPutTransformsResponseSchema = arg => {
  return isBasicObject(arg) && {}.hasOwnProperty.call(arg, 'transformsCreated') && {}.hasOwnProperty.call(arg, 'errors') && Array.isArray(arg.transformsCreated) && Array.isArray(arg.errors);
};

exports.isPutTransformsResponseSchema = isPutTransformsResponseSchema;

const isGenericSuccessResponseSchema = arg => isBasicObject(arg) && Object.values(arg).every(d => ({}).hasOwnProperty.call(d, 'success'));

const isStartTransformsResponseSchema = arg => {
  return isGenericSuccessResponseSchema(arg);
};

exports.isStartTransformsResponseSchema = isStartTransformsResponseSchema;

const isStopTransformsResponseSchema = arg => {
  return isGenericSuccessResponseSchema(arg);
};

exports.isStopTransformsResponseSchema = isStopTransformsResponseSchema;